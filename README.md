# Dpu Pxe Boot

This repository is an example on booting and installing a Bluefield2 DPU over PXE.

## Getting started
A Bluefield2 DPU can be installed through PXE. This has the advantage that the card can be re-installed over the OOB management network without direct access to the host. If necessary, the OS can be changed to meet use-case specific requirements.

To set this up, you would need:
 - A tftp server
 - A dhcp server in the same vlan/l2 segment (or through dhcp relay) as the DPU OOB interface
 - Change the boot order of the DPU UEFI
 - Extract the images from the BFB installation file

## TFTP server and config
There are many implementations of TFTP servers. In this case we are using tftpd-hpa which is installable in your favorite Linux distribution. In Ubuntu it can be installed with `apt install tftpd-hpa`. After installation the daemon should be running, check with `systemctl status tftpd-hpa`.

The default configuration can be found in `/etc/default/tftpd-hpa` and the following is an example configuration. Tftp has many possible options, but the example configuration is a minimalistic example that works in this scenario.

```
# /etc/default/tftpd-hpa

TFTP_USERNAME="tftp"
TFTP_DIRECTORY="/tftp"
TFTP_ADDRESS=":69"
TFTP_OPTIONS="--secure -B 1468 -vvvv"
```

Make sure that the /tftp directory exists and is owned by `tftp` (`chown tftp:tftp /tftp`) and restart tftpd if the configuration file has changed (`systemctl restart tftpd-hpa`).

## Unpack BFB
Use the `mlx-mkbfb` tool to unpack the BFB image (e.g DOCA 1.2). Copy the `dump-image-v0` and `dump-initramfs-v0` files to the tftp directory. The rest of the files can be ignored. In this repository you also find the EFI boot files that need to be included (`BOOTAA64.EFI` and `grubaa64.efi`) with an example grub.cfg.

All files need to be placed in the `/tftp` directory.

## DHCP configuration
The following is an example configuration the ISC dhcp server. In this case we are providing a specific DPU (based on the OOB mac-address). The important parts are providing the `next-server` option and the `filename` which is the bootable EFI file that will load Grub.

```
subnet 10.1.1.0 netmask 255.255.255.0 {
  range 10.1.1.100 10.1.1.200;
  option routers 10.1.1.1;
  option domain-name-servers 10.1.1.1;
  default-lease-time 86400;
  max-lease-time 172800;
}
```

```
  host dpu01 {
    hardware ethernet b8:ce:f6:91:4c:5c;
    fixed-address 10.1.100.51;
    option host-name "dpu01";
    next-server 10.1.100.1;
    filename "BOOTAA64.EFI";
  }
```

## PXE install walk through
A Bluefield2 DPU card by default does not boot from PXE. To enable this the boot order of the card should be changed. This can be done by changing the boot order in the UEFI menu as shown in the [screencapture](videos/bootorder-change.mov).

To find the correct interface to boot PXE (on IPv4) over, look at the boot manager menu. This lists the mac address on the top right and specifies if it is PXE, HTTP, IPv4 or IPv6. This interface should be places on top of the boot order.

If the Bluefield2 card is already installed, this can also be done from the OS. First list the current boot order:
```
ubuntu@dpu01:~$ efibootmgr -v
BootCurrent: 0000
Timeout: 3 seconds
BootOrder: 0000,0003,7470,0001,0005,0006,0004,0007,0008,0009,000A,000B,000C,000D,000E,000F,0010,0011,0012,0013,0014,0015,0016
Boot0000* focal0        HD(1,GPT,24667d30-a074-c943-9f6a-fd11b3b8cd1b,0x800,0x19000)/File(\EFI\ubuntu\shimaa64.efi)
Boot0001* Linux from mmc0       VenHw(8c91e049-9bf9-440e-bbad-7dc5fc082c02)/HD(1,GPT,3dcadb7e-bccc-4897-a766-3c070edd7c25,0x800,0x4a800)/File(Image)c.o.n.s.o.l.e.=.t.t.y.A.M.A.1. .c.o.n.s.o.l.e.=.h.v.c.0. .c.o.n.s.o.l.e.=.t.t.y.A.M.A.0. .e.a.r.l.y.c.o.n.=.p.l.0.1.1.,.0.x.0.1.0.0.0.0.0.0. .e.a.r.l.y.c.o.n.=.p.l.0.1.1.,.0.x.0.1.8.0.0.0.0.0. . .r.o.o.t.=./.d.e.v./.m.m.c.b.l.k.0.p.2. .r.o.o.t.w.a.i.t...
Boot0002* EFI Internal Shell    MemoryMapped(11,0xfdfe4000,0xfe7d977f)/FvFile(7c04a583-9e3e-4f1c-ad65-e05268d0b4d1)
Boot0003* ubuntu        HD(1,GPT,24667d30-a074-c943-9f6a-fd11b3b8cd1b,0x800,0x19000)/File(\EFI\ubuntu\shimaa64.efi)
Boot0004* EFI Internal Shell    MemoryMapped(11,0xfdfe6000,0xfe7dab3f)/FvFile(7c04a583-9e3e-4f1c-ad65-e05268d0b4d1)
Boot0005* Linux from eMMC       VenHw(8c91e049-9bf9-440e-bbad-7dc5fc082c02)/File(Image)c.o.n.s.o.l.e.=.t.t.y.A.M.A.0. .e.a.r.l.y.c.o.n.=.p.l.0.1.1.,.0.x.0.1.0.0.0.0.0.0. .i.n.i.t.r.d.=.i.n.i.t.r.a.m.f.s...
Boot0006* EFI Misc Device       VenHw(8c91e049-9bf9-440e-bbad-7dc5fc082c02)
Boot0007* EFI Network   PciRoot(0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/MAC(b8cef6914c54,1)/IPv4(0.0.0.00.0.0.0,0,0)
Boot0008* EFI Network 1 PciRoot(0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/MAC(b8cef6914c54,1)/IPv6([::]:<->[::]:,0,0)
Boot0009* EFI Network 2 PciRoot(0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/MAC(b8cef6914c54,1)/IPv4(0.0.0.00.0.0.0,0,0)/Uri()
Boot000A* EFI Network 3 PciRoot(0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/MAC(b8cef6914c54,1)/IPv6([::]:<->[::]:,0,0)/Uri()
Boot000B* EFI Network 4 PciRoot(0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x1)/MAC(b8cef6914c55,1)/IPv4(0.0.0.00.0.0.0,0,0)
Boot000C* EFI Network 5 PciRoot(0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x1)/MAC(b8cef6914c55,1)/IPv6([::]:<->[::]:,0,0)
Boot000D* EFI Network 6 PciRoot(0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x1)/MAC(b8cef6914c55,1)/IPv4(0.0.0.00.0.0.0,0,0)/Uri()
Boot000E* EFI Network 7 PciRoot(0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x0,0x1)/MAC(b8cef6914c55,1)/IPv6([::]:<->[::]:,0,0)/Uri()
Boot000F* EFI Network 8 MAC(001acaffff01,1)/IPv4(0.0.0.00.0.0.0,0,0)
Boot0010* EFI Network 9 MAC(001acaffff01,1)/IPv6([::]:<->[::]:,0,0)
Boot0011* EFI Network 10        MAC(001acaffff01,1)/IPv4(0.0.0.00.0.0.0,0,0)/Uri()
Boot0012* EFI Network 11        MAC(001acaffff01,1)/IPv6([::]:<->[::]:,0,0)/Uri()
Boot0013* EFI Network 12        MAC(b8cef6914c5c,1)/IPv4(0.0.0.00.0.0.0,0,0)
Boot0014* EFI Network 13        MAC(b8cef6914c5c,1)/IPv6([::]:<->[::]:,0,0)
Boot0015* EFI Network 14        MAC(b8cef6914c5c,1)/IPv4(0.0.0.00.0.0.0,0,0)/Uri()
Boot0016* EFI Network 15        MAC(b8cef6914c5c,1)/IPv6([::]:<->[::]:,0,0)/Uri()
Boot7470* Linux from rshim      VenHw(f019e406-8c9c-11e5-8797-001aca00bfc4)/File(Image)c.o.n.s.o.l.e.=.t.t.y.A.M.A.1. .c.o.n.s.o.l.e.=.h.v.c.0. .c.o.n.s.o.l.e.=.t.t.y.A.M.A.0. .e.a.r.l.y.c.o.n.=.p.l.0.1.1.,.0.x.0.1.0.0.0.0.0.0. .e.a.r.l.y.c.o.n.=.p.l.0.1.1.,.0.x.0.1.8.0.0.0.0.0. .i.n.i.t.r.d.=.i.n.i.t.r.a.m.f.s. .q.u.i.e.t...
```

Move the PXE IPv4 interface to the first in the list:
```
ubuntu@dpu01:~$ sudo efibootmgr -o 0013,0000,0003,7470,0001,0005,0006,0004,0007,0008,0009,000A,000B,000C,000D,000E,000F,0010,0011,0012,0014,0015,0016
BootCurrent: 0000
Timeout: 3 seconds
BootOrder: 0013,0000,0003,7470,0001,0005,0006,0004,0007,0008,0009,000A,000B,000C,000D,000E,000F,0010,0011,0012,0014,0015,0016
```

When the boot order has been configured correctly, the card can be rebooted (or select the "continue" option) which will start the boot process. As can be seen in the [video](videos/pxe-boot-install.mov), Grub will load and the image will be loaded. Downloading the image file over TFTP can take up to 10 minutes (no console output during the download). Loading the kernel and the installation process another 10 minutes.

Installation over PXE is completed when the login prompt shows. After install the bootorder is changed back so the next boot will be from the newly installed OS.
